/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Management;

/**
 *
 * @author Rhenan
 */
public class Market {

    private int market_id;
    private String market_image;
    private String market_address;
    private String market_country;
    private String market_cnpj; //Só para o BR
    private String market_telephone;
    private String market_longitude;
    private String market_latitude;//Localização do supermercado.
    private double market_popularity;
    private String market_name;//Nome do Supermercado

    public Market() {
    }

    public Market(String market_image, String market_address, String market_country, String market_cnpj, String market_telephone, String market_longitude, String market_latitude, double market_popularity, String market_name) {
        this.market_image = market_image;
        this.market_address = market_address;
        this.market_country = market_country;
        this.market_cnpj = market_cnpj;
        this.market_telephone = market_telephone;
        this.market_longitude = market_longitude;
        this.market_latitude = market_latitude;
        this.market_popularity = market_popularity;
        this.market_name = market_name;
    }

    public int getMarket_id() {
        return market_id;
    }

    public void setMarket_id(int market_id) {
        this.market_id = market_id;
    }

    public String getMarket_image() {
        return market_image;
    }

    public void setMarket_image(String market_image) {
        this.market_image = market_image;
    }

    public String getMarket_address() {
        return market_address;
    }

    public void setMarket_address(String market_address) {
        this.market_address = market_address;
    }

    public String getMarket_country() {
        return market_country;
    }

    public void setMarket_country(String market_country) {
        this.market_country = market_country;
    }

    public String getMarket_cnpj() {
        return market_cnpj;
    }

    public void setMarket_cnpj(String market_cnpj) {
        this.market_cnpj = market_cnpj;
    }

    public String getMarket_telephone() {
        return market_telephone;
    }

    public void setMarket_telephone(String market_telephone) {
        this.market_telephone = market_telephone;
    }

    public String getMarket_longitude() {
        return market_longitude;
    }

    public void setMarket_longitude(String market_longitude) {
        this.market_longitude = market_longitude;
    }

    public String getMarket_latitude() {
        return market_latitude;
    }

    public void setMarket_latitude(String market_latitude) {
        this.market_latitude = market_latitude;
    }

    public double getMarket_popularity() {
        return market_popularity;
    }

    public void setMarket_popularity(double market_popularity) {
        this.market_popularity = market_popularity;
    }

    public String getMarket_name() {
        return market_name;
    }

    public void setMarket_name(String market_name) {
        this.market_name = market_name;
    }


    
}
