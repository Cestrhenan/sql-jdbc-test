/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Management;

/**
 *
 * @author Rhenan
 */
public class User_Market {
    
    private Market market; 
    private User user; 
    private int purchasingnumber; 

    public User_Market() {

    }

    public User_Market(Market market, User user, int purchasingnumber) {
        this.market = market;
        this.user = user;
        this.purchasingnumber = purchasingnumber;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getPurchasingnumber() {
        return purchasingnumber;
    }

    public void setPurchasingnumber(int purchasingnumber) {
        this.purchasingnumber = purchasingnumber;
    }
}