/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Management;

/**
 *
 * @author Rhenan
 */
public class Product_Market {
    
    private String market; 
    private String product; 
    private float actualprice; 
    private long requestnumber;
    private float secondaryprice; 
    private String lastupdate; 
    private float variationtax; 
    private float media; 

    public Product_Market() {
    }

    public Product_Market(String market, String product, float actualprice, long requestnumber, float secondaryprice, String lastupdate, float variationtax, float media, Market marketindice) {
        this.market = market;
        this.product = product;
        this.actualprice = actualprice;
        this.requestnumber = requestnumber;
        this.secondaryprice = secondaryprice;
        this.lastupdate = lastupdate;
        this.variationtax = variationtax;
        this.media = media;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public float getActualprice() {
        return actualprice;
    }

    public void setActualprice(float actualprice) {
        this.actualprice = actualprice;
    }

    public long getRequestnumber() {
        return requestnumber;
    }

    public void setRequestnumber(long requestnumber) {
        this.requestnumber = requestnumber;
    }

    public float getSecondaryprice() {
        return secondaryprice;
    }

    public void setSecondaryprice(float secondaryprice) {
        this.secondaryprice = secondaryprice;
    }

    public String getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(String lastupdate) {
        this.lastupdate = lastupdate;
    }

    public float getVariationtax() {
        return variationtax;
    }

    public void setVariationtax(float variationtax) {
        this.variationtax = variationtax;
    }

    public float getMedia() {
        return media;
    }

    public void setMedia(float media) {
        this.media = media;
    }
  
    
    
}
