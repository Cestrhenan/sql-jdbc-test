/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Management;

/**
 *
 * @author Rhenan
 */
public class Product {

    private String product_barcode;//Codigo de Barra
    private double product_evaluation;
    private String product_image;
    private String product_name;//Nome do Produto
    private String product_brand;
    private String product_tags;
    private double product_popularity;
    private String product_countries;

    public Product() {

    }

    public Product(String product_barcode, double product_evaluation, String product_image, String product_name, String product_brand, String product_tags, double product_popularity, String product_countries) {
        this.product_barcode = product_barcode;
        this.product_evaluation = product_evaluation;
        this.product_image = product_image;
        this.product_name = product_name;
        this.product_brand = product_brand;
        this.product_tags = product_tags;
        this.product_popularity = product_popularity;
        this.product_countries = product_countries;
    }

    public String getProduct_barcode() {
        return product_barcode;
    }

    public void setProduct_barcode(String product_barcode) {
        this.product_barcode = product_barcode;
    }

    public double getProduct_evaluation() {
        return product_evaluation;
    }

    public void setProduct_evaluation(double product_evaluation) {
        this.product_evaluation = product_evaluation;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_brand() {
        return product_brand;
    }

    public void setProduct_brand(String product_brand) {
        this.product_brand = product_brand;
    }

    public String getProduct_tags() {
        return product_tags;
    }

    public void setProduct_tags(String product_tags) {
        this.product_tags = product_tags;
    }

    public double getProduct_popularity() {
        return product_popularity;
    }

    public void setProduct_popularity(double product_popularity) {
        this.product_popularity = product_popularity;
    }

    public String getProduct_countries() {
        return product_countries;
    }

    public void setProduct_countries(String product_countries) {
        this.product_countries = product_countries;
    }
    
    

}
