/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Management;

/**
 *
 * @author Rhenan
 */
public class Purchasing {
 
    private int acquirednumber; 
    private String date; 
    private String list_id;
    private Product product_barcode; 
    private Market market_id;

    public Purchasing() {
    }
    
    public Purchasing(int acquirednumber, String date, String list_id, Product product_barcode, Market market_id) {
        this.acquirednumber = acquirednumber;
        this.date = date;
        this.list_id = list_id;
        this.product_barcode = product_barcode;
        this.market_id = market_id;
    }

       public int getAcquirednumber() {
        return acquirednumber;
    }

    public void setAcquirednumber(int acquirednumber) {
        this.acquirednumber = acquirednumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getList_id() {
        return list_id;
    }

    public void setList_id(String list_id) {
        this.list_id = list_id;
    }

    public Product getProduct_barcode() {
        return product_barcode;
    }

    public void setProduct_barcode(Product product_barcode) {
        this.product_barcode = product_barcode;
    }

    public Market getMarket_id() {
        return market_id;
    }

    public void setMarket_id(Market market_id) {
        this.market_id = market_id;
    }
   
    
}
