/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Management;

/**
 *
 * @author Rhenan
 */
public class User {

    private String user_nick;
    private String user_name;
    private String user_email;
    private String user_image;
    private String user_birthday;
    private String user_gender;
    private String user_cpf; // só BR
    private String user_zipcode;
    private String user_adress; // captado pelo zipcode/gps em tempo de execução
    private String user_password;

    public User() {
    }

    public User(String user_nick, String user_name, String user_email, String user_image, String user_birthday, String user_gender, String user_cpf, String user_zipcode, String user_adress, String user_password) {
        this.user_nick = user_nick;
        this.user_name = user_name;
        this.user_email = user_email;
        this.user_image = user_image;
        this.user_birthday = user_birthday;
        this.user_gender = user_gender;
        this.user_cpf = user_cpf;
        this.user_zipcode = user_zipcode;
        this.user_adress = user_adress;
        this.user_password = user_password;
    }

    public String getUser_nick() {
        return user_nick;
    }

    public void setUser_nick(String user_nick) {
        this.user_nick = user_nick;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_birthday() {
        return user_birthday;
    }

    public void setUser_birthday(String user_birthday) {
        this.user_birthday = user_birthday;
    }

    public String getUser_gender() {
        return user_gender;
    }

    public void setUser_gender(String user_gender) {
        this.user_gender = user_gender;
    }

    public String getUser_cpf() {
        return user_cpf;
    }

    public void setUser_cpf(String user_cpf) {
        this.user_cpf = user_cpf;
    }

    public String getUser_zipcode() {
        return user_zipcode;
    }

    public void setUser_zipcode(String user_zipcode) {
        this.user_zipcode = user_zipcode;
    }

    public String getUser_adress() {
        return user_adress;
    }

    public void setUser_adress(String user_adress) {
        this.user_adress = user_adress;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    
}
