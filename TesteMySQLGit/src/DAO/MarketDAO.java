/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import Connection.ConnectionFactory;
import java.sql.Connection;
import Management.Market;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author Rhenan
 */
public class MarketDAO {
    
    private Connection con = null; 

    public MarketDAO() {
        con = ConnectionFactory.getConnection();
    }
    
    public boolean save(Market market) {
        
        String sql = "INSERT INTO supermercado (Imagem, Endereco, Pais, CNPJ, Telefone, Longitude, Latitude, Popularidade, Nome) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, market.getMarket_image());
            stmt.setString(2, market.getMarket_address());
            stmt.setString(3, market.getMarket_country());
            stmt.setString(4, market.getMarket_cnpj());
            stmt.setString(5, market.getMarket_telephone());
            stmt.setString(6, market.getMarket_longitude());
            stmt.setString(7, market.getMarket_latitude());
            stmt.setDouble(8, market.getMarket_popularity());
            stmt.setString(9, market.getMarket_name());
            //stmt.setString(10, market.getMarket_id()); //é auto-gerada então não precisa ser setada
            stmt.executeUpdate();
            return true;

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            return false;
        }finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
          
    }
    
    public List<Market> findAll (){
    
        
    String sql = "SELECT * FROM supermercado";

    PreparedStatement stmt = null; 
    ResultSet rs = null; 
    
    List<Market> markets= new ArrayList<>();
 
        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {

                Market market = new Market();
                market.setMarket_image(rs.getString("Imagem"));
                market.setMarket_address(rs.getString("Endereco"));
                market.setMarket_country(rs.getString("Pais"));
                market.setMarket_cnpj(rs.getString("CNPJ"));
                market.setMarket_telephone(rs.getString("Telefone"));
                market.setMarket_longitude(rs.getString("Longitude"));
                market.setMarket_latitude(rs.getString("Latitude"));
                market.setMarket_popularity(rs.getDouble("Popularidade"));
                market.setMarket_name(rs.getString("Nome"));
                market.setMarket_id(rs.getInt("idSupermercado"));
                markets.add(market);
                
            }

        } catch (SQLException ex) {
            
            System.err.println("Erro:" + ex);
            
        }finally {
            
            ConnectionFactory.closeConnection(con, stmt, rs);
            
        }
        
        return markets;
    }

    public List<Market> findByName(String name) {
        
    String cmd = "%"+name+"%";
    
    String sql = "SELECT Nome FROM supermercado WHERE Nome like '"+cmd+"'";
    

    PreparedStatement stmt = null; 
    ResultSet rs = null; 
    
    List<Market> markets= new ArrayList<>();
 
        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {

                Market market = new Market();
                market.setMarket_name(rs.getString("Nome"));
                markets.add(market);
            }

        } catch (SQLException ex) {
            
            System.err.println("Erro:" + ex);
            
        }finally {
            
            ConnectionFactory.closeConnection(con, stmt, rs);
            
        }
        return markets;
    }
    
    public List<Market> findByAdress (String adress) {
    
        String sql = "SELECT Nome FROM supermercado WHERE Nome like '"+adress+"'";
    

    PreparedStatement stmt = null; 
    ResultSet rs = null; 
    
    List<Market> markets= new ArrayList<>();
 
        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {

                Market market = new Market();
                market.setMarket_name(rs.getString("Endereco"));
                markets.add(market);
            }

        } catch (SQLException ex) {
            
            System.err.println("Erro:" + ex);
            
        }finally {
            
            ConnectionFactory.closeConnection(con, stmt, rs);
            
        }
        return markets;
    }

    public boolean updateMarket (Market market) {
     
        
    String sql = "UPDATE supermercado SET Nome = ? , Endereco = ? WHERE idSupermercado = ?";

    PreparedStatement stmt = null; 
    
    
    try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, market.getMarket_name());
            stmt.setString(2, market.getMarket_address());
            stmt.setDouble(3, market.getMarket_id());
            stmt.executeUpdate();
            return true;

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            return false;
        }finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
    
    }
    
    public boolean deleteMarket (Market market) {
    
    String sql = "DELETE FROM supermercado where idSupermercado = ?";

    PreparedStatement stmt = null; 
    
    
    try {
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, market.getMarket_id());
            stmt.executeUpdate();
            return true;

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            return false;
        }finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
    
    }



}





   
   
