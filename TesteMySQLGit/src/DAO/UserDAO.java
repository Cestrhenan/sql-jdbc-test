/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Connection.ConnectionFactory;
import Management.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Rhenan
 */
public class UserDAO {
    
      private Connection con = null; 

    public UserDAO() {
        con = ConnectionFactory.getConnection();
    }
    
    public boolean save(User user) {
        
        String sql = "INSERT INTO usuario (Nick, Nome, Email, Imagem, Nascimento, Genero, CPF, CEP, Endereço, Senha) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, user.getUser_nick());
            stmt.setString(2, user.getUser_name());
            stmt.setString(3, user.getUser_email());
            stmt.setString(4, user.getUser_image());
            stmt.setString(5, user.getUser_birthday());
            stmt.setString(6, user.getUser_gender());
            stmt.setString(7, user.getUser_cpf());
            stmt.setString(8, user.getUser_zipcode());
            stmt.setString(9, user.getUser_adress());
            stmt.setString(10,user.getUser_password());
            stmt.executeUpdate();
            return true;

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            return false;
        }finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
          
    }
}