/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Connection.ConnectionFactory;
import Management.User;
import Management.Market;
import Management.User_Market;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Rhenan
 */
public class User_MarketDAO {
    
    private Connection con = null; 
    
        public User_MarketDAO() {
            con = ConnectionFactory.getConnection();
    }
    
    public boolean save(User_Market user_market) {

        String sql = "INSERT INTO `usuario-supermercado` (QntComprasRealizadas, Supermercado_idSupermercado, usuario_Nick) VALUES (?, ?, ?)";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setInt(1, user_market.getPurchasingnumber());
            stmt.setInt(2, user_market.getMarket().getMarket_id());
            stmt.setString(3, user_market.getUser().getUser_nick());
            stmt.executeUpdate();
            return true;

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            return false;
        }finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
          
    }
    
}
