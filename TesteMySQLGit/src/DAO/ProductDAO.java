/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Connection.ConnectionFactory;
import Management.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rhenan
 */
public class ProductDAO {
    
      private Connection con = null; 

    public ProductDAO() {
        con = ConnectionFactory.getConnection();
    }
    
    public boolean save(Product product) {
        
        String sql = "INSERT INTO produto (CodigoDeBarras14, Avaliação, Imagem, Nome, Marca, Popularidade, Tags, Países) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, product.getProduct_barcode());
            stmt.setDouble(2, product.getProduct_evaluation());
            stmt.setString(3, product.getProduct_image());
            stmt.setString(4, product.getProduct_name());
            stmt.setString(5, product.getProduct_brand());
            stmt.setDouble(6, product.getProduct_popularity());
            stmt.setString(7, product.getProduct_tags());
            stmt.setString(8, product.getProduct_countries());
            stmt.executeUpdate();
            return true;

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            return false;
        }finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
          
    }
    
 public List<Product> findByName(String name) {
        
    String cmd = "%"+name+"%";
    
    String sql = "SELECT Nome FROM supermercado WHERE Nome like '"+cmd+"'";
    

    PreparedStatement stmt = null; 
    ResultSet rs = null; 
    
    List<Product> products= new ArrayList<>();
 
        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {

                Product product = new Product();
                product.setProduct_name(rs.getString("Nome"));
                products.add(product);
            }

        } catch (SQLException ex) {
            
            System.err.println("Erro:" + ex);
            
        }finally {
            
            ConnectionFactory.closeConnection(con, stmt, rs);
            
        }
        return products;
    }
     
    
}
