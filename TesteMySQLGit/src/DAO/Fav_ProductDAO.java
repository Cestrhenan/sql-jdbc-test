/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Connection.ConnectionFactory;
import Management.Fav_Product;
import Management.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rhenan
 */
public class Fav_ProductDAO {

    private Connection con = null;

    //LEMBRAR DE COLOCAR A CONEXÃO NO CONSTRUTOR
    public Fav_ProductDAO() {
        con = ConnectionFactory.getConnection();

    }

    public boolean saveFav_Product(Fav_Product favproduct) {

        String sql = "INSERT INTO Produto_Favorito (Produto_CodigoDeBarras14, usuario_Nick) VALUES (?, ?)";

        PreparedStatement stmt = null;

        try {

            stmt = con.prepareStatement(sql);
            stmt.setString(1, favproduct.getProduct());
            stmt.setString(2, favproduct.getUser());
            stmt.executeUpdate();
            return true;

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            return false;
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }

    }

    public List<Fav_Product> findAllByUser(String user) {

        String sql = "SELECT * FROM produto_favorito WHERE usuario_Nick = '" + user + "'";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<Fav_Product> fav_products = new ArrayList<>();

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {

                Fav_Product fav_product = new Fav_Product();
                fav_product.setProduct(rs.getString("Produto_CodigoDeBarras14"));
                fav_product.setUser(rs.getString("usuario_Nick"));
                fav_products.add(fav_product);
            }
        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }
        return fav_products;
    }

    public List<Fav_Product> findAllByProduct(String product) {

        String sql = "SELECT * FROM produto_favorito WHERE Produto_CodigoDeBarras14 = '" + product + "'";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Fav_Product> fav_products = new ArrayList<>();

        try {
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {

                Fav_Product fav_product = new Fav_Product();
                fav_product.setProduct(rs.getString("Produto_CodigoDeBarras14"));
                fav_product.setUser(rs.getString("usuario_Nick"));
                fav_products.add(fav_product);
            }
        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }
        return fav_products;
    }
    
    public boolean deleteFav_Product (Fav_Product favproduct) {
    
    String sql = "DELETE FROM produto_favorito WHERE Produto_CodigoDeBarras14 = ? and usuario_Nick = ?";

    PreparedStatement stmt = null; 
  
    try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, favproduct.getProduct());
            stmt.setString(2, favproduct.getUser());
            stmt.executeUpdate();
            return true;

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            return false;
        }finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
  
    }

}
