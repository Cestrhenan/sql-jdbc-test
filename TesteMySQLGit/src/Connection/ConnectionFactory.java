package Connection;

import java.sql.Connection;
import com.mysql.jdbc.PreparedStatement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rhenan
 */
public class ConnectionFactory {
    //utilize essas variaveis para mudar a conexão
    private static final String Driver = "com.mysql.jdbc.Driver";
    private static final String Url = "jdbc:mysql://localhost:3306/projeto-x";
    private static final String User = "root";
    private static final String Password = "";
    
    public static Connection getConnection() {
        
        try {
            Class.forName(Driver);
            return DriverManager.getConnection(Url, User, Password);
            
        } catch (ClassNotFoundException | SQLException ex) {
            
            throw new RuntimeException("Connection Error", ex);
            
        }
        
    }
    
    public static void closeConnection(Connection con) {
        
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                System.err.println("Error:" +ex);
            }
        }
    }

    public static void closeConnection(Connection con, java.sql.PreparedStatement stmt) {
        
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                System.err.println("Error:" +ex);
            }
        }
        closeConnection(con);
    }
    
    public static void closeConnection(Connection con, java.sql.PreparedStatement stmt, ResultSet rs) {
        
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                System.err.println("Error:" +ex);
            }
        }
        closeConnection(con,stmt);
    }
    
}
